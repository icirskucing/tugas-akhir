RULES:
HANYA DIPERBOLEHKAN MELIHAD MODUL. JANGAN MELIHAT DARI FILE SEBELUMNYA!!!!!
DIKUMPULKAN HARI INI SAMPAI PRAKTIKUM SELESAI
DIPERBOLEHKAN MENGGANGGU ASISTEN DENGAN ALASAN YANG WAJAR
ABSENSI BERDASARKAN BRANCH YANG ADA DI GITLAB

PENILAIAN : 
- Yang bisa menyelesaikan sebelum asisten nilai = 100
- Yang membantu temannya(dengan syarat dia sudah selesai) mendapat nilai tambahan dengan cara menambakan namanya dalam branch temannya(Format: NIM_NAMA_NAMA TEMAN). Hanya boleh dibantu oleh satu orang
- Yang tidak menyelesaikan, tidak akan mendapatkan nilai
- Yang baru sebagian akan mendapat nilai effort sesuai dengan effortnya
- Diwajibkan menyelesaikan sampain datatable. Akan ada bonus jika menyelesaikan sampai grafik

Nama Branch : NIM_NAMA

GUNAKAN KONSEP OOP DAN DAO!!!!
Cek:
Database dan query akses web
1. Persiapkan database dan koneksi ke web
2. Persiapkan DML untuk diakses di web

Lihat table kegiatan
1. Buat index.php untuk melihat keseluruhan tabel
2. Tambahkan tombol untuk menambah data

Input Form
1. Buat form_kegiatan.php untuk melakukan input, update, delete

Proses
1. Buat proses_kegiatan.php untuk memproses inputan dari form

View
1. Buat view_kegiatan.php untuk menampilkan data secara detail

JQUERY:
1. Download file jquery di elen. Lalu pindahkan sesuai tipe file
.css -> folder css
.js -> folder js

JQUERY VALIDATION: 
1. Import file js(tentang validation) kedalam project
note: import semua file ke bagian atas karena harus terload duluan!!!
2. Beri nama pada form_kegiatan untuk diakses jquery
3. Buat file untuk memvalidasi(form-validasi.js) dalam folder js
4. Isi file untuk memvalidasi input form(instruksi selanjutnya ada di file)
5. Panggil file yang telah dibuat kedalam form

JQUERY DATA TABLES:
1. Import file css dan js(tentang data tables)
note: import semua file ke bagian atas karena harus terload duluan!!!
2. Modifikasi index.php
Note: keterangan berikutnya ada di source code

JQUERY GRAFIK:
1. Siapkan query untuk mendapatkan data untuk GRAFIK
2. Query yang sudah dipersiapkan, masukkan kedalam class DML berbentuk fungsi
3. Buat file(grafik_kegiatan.php) untuk menampilkan grafik_kegiatan
Note: Perintah selanjutnya ada dlm file