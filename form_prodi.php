<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_prodi.php';
    //buat variabel untuk memanggil class
    $obj_prodi = new Prodi();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_prodi->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_prodi.js"></script>
<form name="form_prodi" class="form-horizontal" method="POST" action="proses_prodi.php">
<fieldset>

<!-- Form Name -->
<legend>Form Prodi</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Kode</label>
  <div class="col-md-4">
  <input id="kode" name="kode" type="text" placeholder="Masukkan Kode" class="form-control input-md" value="<?php echo $data['kode']?>">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>" >

  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
