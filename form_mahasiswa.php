<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_mahasiswa.php';
    //buat variabel untuk memanggil class
    $obj_mahasiswa = new Mahasiswa();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['nim'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_mahasiswa->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_mahasiswa.js"></script>
<form name="form_mahasiswa" class="form-horizontal" method="POST" action="proses_mahasiswa.php">
<fieldset>

<!-- Form Name -->
<legend>Form Mahasiswa</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nim">NIM</label>
  <div class="col-md-4">
  <input id="nim" name="nim" type="text" placeholder="Masukkan NIM" class="form-control input-md" value="<?php echo $data['nim']?>">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>" >

  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="jk">Jenis Kelamin</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="jk-0">
      <input name="jk" id="jk-0" value="L" checked="checked" type="radio">
      Laki-Laki
    </label>
	</div>
  <div class="radio">
    <label for="jk-1">
      <input name="jk" id="jk-1" value="P" type="radio">
      Perempuan
    </label>
	</div>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="prodi_id">Prodi Id</label>
  <div class="col-md-4">
  <input id="prodi_id" name="prodi_id" type="text" placeholder="Masukkan Prodi ID" class="form-control input-md" value="<?php echo $data['prodi_id']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="thnmasuk">Tahun Masuk</label>
  <div class="col-md-4">
  <input id="thnmasuk" name="thnmasuk" type="text" placeholder="Masukkan Tahun Masuk" class="form-control input-md" value="<?php echo $data['thnmasuk']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rombel_id">Rombel ID</label>
  <div class="col-md-4">
  <input id="rombel_id" name="rombel_id" type="text" placeholder="Masukkan Rombongan Belajar ID" class="form-control input-md" value="<?php echo $data['rombel_id']?>" >

  </div>
</div>
<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
