<?php
  include_once 'top.php';
	require_once 'db/class_prodi.php';
?>
<h2>DAFTAR Prodi</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_prodi.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Prodi
    </a>
</div>
<?php
	$obj_prodi = new Prodi();
	$rows = $obj_prodi->getAll ();
?>

<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>

	<table class="table">
		<thead>
			<tr class="active">
				<th> Id </th>
        <th> Kode</th>
        <th> Nama</th>
        <th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$nomor = 1;
				foreach ($rows as $row){
					echo '<tr><td>'.$row['id'].'</td>';
					echo '<td>'.$row['kode'].'</td>';
					echo '<td>'.$row['nama'].'</td>';
					echo '<td><a href="view_prodi.php?id='.$row['id']. '">View</a> |';
					echo '<a href="form_prodi.php?id='.$row['id']. '">Update</a></td>';
 					echo '</tr>';
					$nomor++;
				}
				?>
		</tbody>
	</table>
<?php
    include_once 'bottom.php';
?>
