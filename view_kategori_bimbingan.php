<?php
    include_once 'top.php';
    require_once 'db/class_kategori_bimbingan.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKategoriBimbingan = new Kategori_bimbingan();
    $_id = $_GET['id'];
    $data = $objKategoriBimbingan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Kategori Bimbingan</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">Id</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">Nama</td><td>:</td><td><?php echo
                $data['nama']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>
