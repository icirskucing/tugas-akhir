<?php
  include_once 'top.php';
	require_once 'db/class_kategori_bimbingan.php';
?>
<h2>DAFTAR KATEGORI BIMBINGAN</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kategori_bimbingan.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kategori
    </a>
</div>
<?php
	$obj_kategori_bimbingan = new Kategori_bimbingan();
	$rows = $obj_kategori_bimbingan->getAll ();
?>

<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>

	<table class="table">
		<thead>
			<tr class="active">
				<th> Id </th>
        <th> Nama</th>
        <th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$nomor = 1;
				foreach ($rows as $row){
					echo '<tr><td>'.$row['id'].'</td>';
					echo '<td>'.$row['nama'].'</td>';
					echo '<td><a href="view_kategori_bimbingan.php?id='.$row['id']. '">View</a> |';
					echo '<a href="form_kategori_bimbingan.php?id='.$row['id']. '">Update</a></td>';
 					echo '</tr>';
					$nomor++;
				}
				?>
		</tbody>
	</table>
<?php
    include_once 'bottom.php';
?>
