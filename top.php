<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>STT Terpadu Nurul Fikri</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

	<link href="css/bootstrap_united.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="js/dataTables.bootstrap.min.js"/>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	<script src="js/jquery.validate.js"></script>


  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="index.php">STTNF</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active">
							<a href="prodi.php">Prodi</a>
						</li>
						<li>
							<a href="mahasiswa.php">Mahasiswa</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bimbingan<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="kategori_bimbingan.php">Kategori Bimbingan</a>
								</li>
								<li>
									<a href="bimbingan_akademik.php">Bimbingan Akademik</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

			</nav>
		</div>
	</div>
