<?php
    include_once 'top.php';
    require_once 'db/class_mahasiswa.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objMahasiswa = new Mahasiswa();
    $_nim = $_GET['nim'];
    $data = $objMahasiswa->findByID($_nim);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Mahasiswa</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">NIM</td><td>:</td><td><?php echo
                $data['nim']?></td>
                </tr>
                <tr>
                <td class="active">Nama</td><td>:</td><td><?php echo
                $data['nama']?></td>
                </tr>
                <tr>
                <td class="active">Tmp Lahir</td><td>:</td><td><?php echo
                $data['tmp_lahir']?></td>
                </tr>
                <tr>
                <td class="active">Tgl Lahir</td><td>:</td><td><?php echo
                $data['tgl_lahir']?></td>
                </tr>
                <tr>
                <td class="active">Jenis Kelamin</td><td>:</td><td><?php echo
                $data['jk']?></td>
                </tr>
                <tr>
                <td class="active">Prodi_Id</td><td>:</td><td><?php echo
                $data['prodi_id']?></td>
                </tr>
                <tr>
                <td class="active">IPK</td><td>:</td><td><?php echo
                $data['ipk']?></td>
                </tr>
                <tr>
                <td class="active">Tahun Masuk</td><td>:</td><td><?php echo
                $data['thnmasuk']?></td>
                </tr>
                <tr>
                <td class="active">Email</td><td>:</td><td><?php echo
                $data['email']?></td>
                </tr>
                <tr>
                <td class="active">Rombel_Id</td><td>:</td><td><?php echo
                $data['rombel_id']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>
