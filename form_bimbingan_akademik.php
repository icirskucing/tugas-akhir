<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_bimbingan_akademik.php';
    //buat variabel untuk memanggil class
    $obj_bimbingan_akademik = new Bimbingan_akademik();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_bimbingan_akademik->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_bimbingan_akademik.js"></script>
<form name="form_bimbingan_akademik" class="form-horizontal" method="POST" action="proses_bimbingan_akademik.php">
<fieldset>

<!-- Form Name -->
<legend>Form Bimbingan Akademik</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
  <div class="col-md-4">
  <input id="tanggal" name="tanggal" type="text" placeholder="Masukkan Tanggal" class="form-control input-md" value="<?php echo $data['tanggal']?>">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="bimbingan_presensi">Bimbingan Presensi</label>
  <div class="col-md-4">
  <input id="bimbingan_presensi" name="bimbingan_presensi" type="text" placeholder="Masukkan Bimbingan Presensi" class="form-control input-md" value="<?php echo $data['bimbingan_presensi']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="bimbingan_keuangan">Bimbingan Keuangan</label>
  <div class="col-md-4">
  <input id="bimbingan_keuangan" name="bimbingan_keuangan" type="text" placeholder="Masukkan Bimbingan Keuangan" class="form-control input-md" value="<?php echo $data['bimbingan_keuangan']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="bimbingan_akademik">Bimbingan Akademik</label>
  <div class="col-md-4">
  <input id="bimbingan_akademik" name="bimbingan_akademik" type="text" placeholder="Masukkan Bimbingan Akademik" class="form-control input-md" value="<?php echo $data['bimbingan_akademik']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori_id">Kategori Id</label>
  <div class="col-md-4">
  <input id="kategori_id" name="kategori_id" type="text" placeholder="Masukkan Kategori ID" class="form-control input-md" value="<?php echo $data['kategori_id']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="semester">Semester</label>
  <div class="col-md-4">
  <input id="semester" name="semester" type="text" placeholder="Masukkan Semester" class="form-control input-md" value="<?php echo $data['semester']?>" >

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nim">NIM</label>
  <div class="col-md-4">
  <input id="nim" name="nim" type="text" placeholder="Masukkan NIM" class="form-control input-md" value="<?php echo $data['nim']?>" >

  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
