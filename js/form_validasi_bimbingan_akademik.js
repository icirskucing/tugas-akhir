//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_bimbingan_akademik']").validate({

        //buat aturan untuk input form
        rules:{
            id: {
              required:true,
              maxLength:10,
            },
            tanggal:"required",
            bimbingan_presensi:"required",
            bimbingan_keuangan:"required",
            bimbingan_akademik:"required",
            kategori_id:"required",
            semester:"required",
            nim:"required",
        },
        //tampilkan pesan
        messages:{
            id: {
              required: "Kode Wajib diisi!!",
              maxLength : "Maksimum 10 charakter",
            },
            tanggal:"Tanggal wajib diisi",
            bimbingan_presensi:"wajib diisi",
            bimbingan_keuangan:"wajib diisi",
            bimbingan_akademik:"wajib diisi",
            kategori_id:"wajib diisi",
            semester:"Semester wajib diisi",
            nim:"NIM wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});
