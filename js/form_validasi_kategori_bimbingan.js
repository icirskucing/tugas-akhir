//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_kategori_bimbingan']").validate({

        //buat aturan untuk input form
        rules:{
            nama:"required",
        },
        //tampilkan pesan
        messages:{
            nama:"Nama wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});
