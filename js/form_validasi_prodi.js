//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_prodi']").validate({

        //buat aturan untuk input form
        rules:{
            id:"required",
            kode:"required",
            nama:"required",
        },
        //tampilkan pesan
        messages:{
            id:"ID wajib diisi!!",
            kode:"Kode wajib diisi",
            nama:"Nama wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});
