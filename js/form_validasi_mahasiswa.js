//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_mahasiswa']").validate({

        //buat aturan untuk input form
        rules:{
            nim:"required",
            nama:"required",
            jk:"required",
            prodi_id:"required",
            thnmasuk:"required",
            rombel_id:"required",
        },
        //tampilkan pesan
        messages:{
            nim:"nim wajib diisi!!",
            nama:"Nama wajib diisi",
            jk:"Jenis Kelamin wajib diisi",
            prodi_id:"Prodi_id wajib diisi",
            thnmasuk:"Tahun Masuk wajib diisi",
            rombel_id:"Rombongan Belajar ID wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});
