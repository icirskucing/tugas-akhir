<?php
  include_once 'top.php';
	require_once 'db/class_mahasiswa.php';
?>
<h2>DAFTAR Mahasiswa</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_mahasiswa.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Mahasiswa
    </a>
    <a class="btn icon-btn btn-success" href="grafik_prodi.php">
    <span class="glyphicon btn-glyphicon img-
    circle text-success"></span>
    Grafik Mahasiswa
    </a>
</div>
<?php
	$obj_mahasiswa = new Mahasiswa();
	$rows = $obj_mahasiswa->getAll ();
?>

<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>

	<table class="table">
		<thead>
			<tr class="active">
				<th> NIM </th>
        <th> Nama</th>
        <th>Jenis Kelamin</th>
        <th>Prodi_Id</th>
        <th>Tahun Masuk</th>
        <th>Rombel_Id</th>
        <th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$nomor = 1;
				foreach ($rows as $row){
					echo '<tr><td>'.$row['nim'].'</td>';
					echo '<td>'.$row['nama'].'</td>';
          echo '<td>'.$row['jk'].'</td>';
          echo '<td>'.$row['prodi_id'].'</td>';
          echo '<td>'.$row['thnmasuk'].'</td>';
          echo '<td>'.$row['rombel_id'].'</td>';
					echo '<td><a href="view_mahasiswa.php?nim='.$row['nim']. '">View</a> |';
					echo '<a href="form_mahasiswa.php?nim='.$row['nim']. '">Update</a></td>';
 					echo '</tr>';
					$nomor++;
				}
				?>
		</tbody>
	</table>
<?php
    include_once 'bottom.php';
?>
