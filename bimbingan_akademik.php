<?php
  include_once 'top.php';
	require_once 'db/class_bimbingan_akademik.php';
?>
<h2>DAFTAR BIMBINGAN AKADEMIK</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_bimbingan_akademik.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Bimbingan
    </a>
</div>

<?php
	$obj = new Bimbingan_akademik();
	$rows = $obj->getAll ();
?>

<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').datatable();
    });
</script>

	<table class="table">
		<thead>
			<tr class="active">
				<th> ID </th>
        <th> Tanggal</th>
        <th> Bimbingan Presensi</th>
        <th> Bimbingan Keuangan</th>
        <th> Bimbingan Akademik</th>
        <th> Kategori Id</th>
        <th> Semester</th>
        <th> NIM</th>
        <th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$nomor = 1;
				foreach ($rows as $row){
					echo '<tr><td>'.$row['id'].'</td>';
					echo '<td>'.$row['tanggal'].'</td>';
					echo '<td>'.$row['bimbingan_presensi'].'</td>';
          echo '<td>'.$row['bimbingan_keuangan'].'</td>';
          echo '<td>'.$row['bimbingan_akademik'].'</td>';
          echo '<td>'.$row['kategori_id'].'</td>';
          echo '<td>'.$row['semester'].'</td>';
          echo '<td>'.$row['nim'].'</td>';
					echo '<td><a href="view_bimbingan_akademik.php?id='.$row['id']. '">View</a> |';
					echo '<a href="form_bimbingan_akademik.php?id='.$row['id']. '">Update</a></td>';
 					echo '</tr>';
					$nomor++;
				}
				?>
		</tbody>
	</table>
<?php
    include_once 'bottom.php';
?>
