<?php
    include_once 'top.php';
    require_once 'db/class_prodi.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objProdi = new Prodi();
    $_id = $_GET['id'];
    $data = $objProdi->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Prodi</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">ID</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">Kode</td><td>:</td><td><?php echo
                $data['kode']?></td>
                </tr>
                <tr>
                <td class="active">Nama</td><td>:</td><td><?php
                echo $data['nama']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>
