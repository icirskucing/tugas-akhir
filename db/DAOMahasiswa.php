<?php
require_once "class_dbkoneksi.php";
class DAOMahasiswa// Data Access Object
{
    protected $tableName = "";
    protected $koneksi = null ;

    public function __construct()
    {
        $this->tableName = 'mahasiswa';
        $database = new DBKoneksi();
        $this->koneksi = $database->getKoneksi();
    }

    public function getAll(){
        $sql = "SELECT * FROM " . $this->tableName;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
    }

    public function hapus($pk){
        $sql = "DELETE FROM mahasiswa" . " WHERE nim=?";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute([$pk]);
        return $ps->rowCount(); // jika 1 sukses
    }

    public function findByID($pk){
      $sql = "SELECT * FROM mahasiswa" . " WHERE nim=?";
      $ps = $this->koneksi->prepare($sql);
      $ps->execute([$pk]);
      return $ps->fetch();
    }

}
