<?php
    /*
    mysql> desc mahasiswa;
    +-----------+-------------+------+-----+---------+-------+
    | Field     | Type        | Null | Key | Default | Extra |
    +-----------+-------------+------+-----+---------+-------+
    | nim       | varchar(10) | NO   | PRI | NULL    |       |
    | nama      | varchar(45) | YES  |     | NULL    |       |
    | tmp_lahir | varchar(30) | YES  |     | NULL    |       |
    | tgl_lahir | date        | YES  |     | NULL    |       |
    | jk        | char(1)     | YES  |     | NULL    |       |
    | prodi_id  | int(11)     | NO   | MUL | NULL    |       |
    | ipk       | double      | YES  |     | NULL    |       |
    | thnmasuk  | int(11)     | YES  |     | NULL    |       |
    | email     | varchar(45) | YES  |     | NULL    |       |
    | rombel_id | int(11)     | NO   | MUL | NULL    |       |
    +-----------+-------------+------+-----+---------+-------+
    10 rows in set (0.15 sec)


    */
    require_once "DAOMahasiswa.php";
    class Mahasiswa extends DAOMahasiswa
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (nim,nama,jk,prodi_id,thnmasuk,rombel_id) ".
            " VALUES (?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nama=?,jk=?,prodi_id=?,thnmasuk=?,rombel_id=?".
            " WHERE nim=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
